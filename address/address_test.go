/*
 Copyright (C) BABEC. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package address

import (
	"testing"
)

func Test_IsValidAddress(t *testing.T) {
	var valid bool
	//len 42 and begin with "0x" is valid address
	valid = IsValidAddress("0x0000000000000000000000000000000000000000")
	if !valid {
		panic("err0")
	}
	// len 40 or len 42 with "0x" or "0X" prefix is valid address
	valid = IsValidAddress("0x0000000000000000000000000000000000000000",
		"0000000000000000000000000000000000000000",
		"0x1000000000000000000000000000000000000000",
		"0X1000000000000000000000000000000000000000",
	)
	if !valid {
		panic("err1")
	}

	//len 42 and begin with "0x" is valid address
	valid = IsValidAddress([]string{}...)
	if valid {
		panic("err0")
	}
	valid = IsValidAddress("0x0000000000000000000000000000000000000")
	if valid {
		panic("err0")
	}
	// len 39 or 41 not valid address
	valid = IsValidAddress("0x000000000000000000000000000000000000000",
		"0000000000000000000000000000000000000000",
		"0X0000000000000000000000000000000000000000")
	if valid {
		panic("err2")
	}
	// len is 42 but not hex string are not valid address
	valid = IsValidAddress("0x00000000k0000000000000000000000000000000",
		"0000000000000000000000000000000000000000",   // valid
		"0X0000000000000000000000000000000000000000", //valid
	)
	if valid {
		panic("err3")
	}

	// len is 42 but not hex string are not valid address
	valid = IsValidAddress("000000000000000000000000000000000000000000",
		"0000000000000000000000000000000000000000",   // valid
		"0X0000000000000000000000000000000000000000", //valid
	)
	if valid {
		panic("err3")
	}
}
