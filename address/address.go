/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package address

import (
	"encoding/hex"
	"fmt"
)

// Address chainmaker address
type Address [20]byte

const addrLen = 40
const addrLenWithPrefix = 42

// ZeroAddr 0
var ZeroAddr = "0000000000000000000000000000000000000000"

// ZeroAddrWithPrefix 0x0
var ZeroAddrWithPrefix = "0x0000000000000000000000000000000000000000"

// IsValidAddress validity verification
func IsValidAddress(addr ...string) bool {
	if len(addr) == 0 {
		return false
	}
	for _, a := range addr {
		if len(a) != addrLen && len(a) != addrLenWithPrefix {
			return false
		}
		if len(a) == addrLenWithPrefix {
			if a[0] != '0' || (a[1] != 'x' && a[1] != 'X') {
				return false
			}
			a = a[2:]
		}
		_, err := hex.DecodeString(a)
		if err != nil {
			return false
		}
	}
	return true
}

// IsZeroAddress return bool
func IsZeroAddress(addr string) bool {
	return addr == ZeroAddr || addr == ZeroAddrWithPrefix
}

// ParseAddress get Address obj from str
func ParseAddress(addrStr string) (*Address, error) {
	addrStrLen := len(addrStr)
	if addrStrLen != addrLen && addrStrLen != addrLenWithPrefix {
		return nil, fmt.Errorf("invalid addr")
	}
	if addrStrLen == addrLenWithPrefix {
		if addrStr[0] != '0' && addrStr[1] != 'x' && addrStr[1] != 'X' {
			return nil, fmt.Errorf("invalid addr")
		}
		addrStr = addrStr[2:]
	}
	addrBytes, err := hex.DecodeString(addrStr)
	if err != nil {
		return nil, fmt.Errorf("invalid addr")
	}
	var ret [20]byte
	copy(ret[:], addrBytes)
	return (*Address)(&ret), nil
}

// GetCleanAddr remove 0x prefix
func GetCleanAddr(addr string) string {
	if len(addr) == addrLenWithPrefix {
		return addr[2:]
	}
	return addr
}

// ToString Address obj to str
func (addr *Address) ToString() string {
	return hex.EncodeToString(addr[:])
}
