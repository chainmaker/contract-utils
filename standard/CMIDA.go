/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package standard

import (
	"time"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
)

// 事件名称
const (
	EventPlatformRegistered      = "PlatformRegistered"
	EventPlatformModified        = "PlatformModified"
	EventUserRegistered          = "UserRegistered"
	EventUserTypeChanged         = "UserTypeChanged"
	EventUserPermissionsChanged  = "UserPermissionsChanged"
	EventUserApprovalChanged     = "UserApprovalChanged"
	EventIDACreated              = "IDACreated"
	EventIDAUpdated              = "IDAUpdated"
	EventIDADeleted              = "IDADeleted"
	EventIDACertificationApplied = "IDACertificationApplied"
	EventIDACertified            = "IDACertified"
)

// 资产信息子结构标识
const (
	FieldIDABasic          = "IDABasic"
	FieldIDAOwnership      = "IDAOwnership"
	FieldIDASource         = "IDASource"
	FieldIDAScenarios      = "IDAScenarios"
	FieldIDASupply         = "IDASupply"
	FieldIDADetails        = "IDADetails"
	FieldIDAStatus         = "IDAStatus"
	FieldIDAColumns        = "IDAColumns"
	FieldIDAAPI            = "IDAApi"
	FieldIDACertifications = "IDACertifications"
)

// CMIDA 长安链可认证数据资产（IDA）合约标准接口
// https://git.code.tencent.com/ChainMaker/contracts/standard/blob/master/draft/CM-CS-240301-IDA.md
type CMIDA interface {

	// 平台管理方法

	// RegisterPlatform 注册一个平台
	// 注册成功时会发出 PlatformRegistered 事件。
	// 如果注册成功，返回平台 id 以及可能的错误代码。
	RegisterPlatform(platform PlatformInfo) protogo.Response

	// ModifyPlatform 修改平台信息，只允许修改企业相关的字段
	// 只允许注册者操作，修改成功时会发出 PlatformModified 事件。
	ModifyPlatform(platform PlatformInfo) protogo.Response

	// 用户管理方法

	// RegisterUser 根据提供的详细信息注册一个新用户。
	// 注册成功时会发出 UserRegistered 事件。
	// 如果注册成功，返回链账户 id 以及可能的错误代码。
	RegisterUser(user UserInfo) protogo.Response

	// ChangeUserType 将现有用户的类型更改为指定的新类型。
	// 需要管理员权限，并在成功时发出 UserTypeChanged 事件。
	// 如果用户类型成功更改，返回 true 以及可能的错误代码。
	ChangeUserType(newType int, userAddress string) protogo.Response

	// ChangeUserPermissions 将现有用户的权限更改为指定的新权限。
	// 需要管理员权限，并在成功时发出 UserPermissionsChanged 事件。
	// 如果用户权限成功更改，返回 true 以及可能的错误代码。
	ChangeUserPermissions(newPermissions int, userAddress string) protogo.Response

	// Approve 授权或取消授权用户操作所有IDA。
	// 成功时发出 UserApprovalChanged 事件。
	// 如果用户授权成功更改，返回 true 以及可能的错误代码。
	Approve(userAddress string, isApproved bool) protogo.Response

	// QueryUser 根据用户地址检索用户信息。
	// 返回包含用户详细信息的 UserInfo 结构体和可能的错误代码。
	QueryUser(userAddress string) protogo.Response

	// QueryApprovedUsers 检索所有授权用户的列表。
	// 返回授权用户地址的列表和可能的错误代码。
	QueryApprovedUsers(userAddress string) protogo.Response

	// QueryUserBalance 检索用户的余额。
	// 返回用户余额数额和可能的错误代码。
	QueryUserBalance(userAddress string) protogo.Response

	// IDA 管理方法

	// CreateIDAs 根据提供的详细信息创建一个新的IDA。
	// 需要登记权限，并在成功时发出 IDACreated 事件。
	// 如果IDA成功创建，返回 true 以及可能的错误代码。
	CreateIDAs(idaInfos []IDAInfo) protogo.Response

	// UpdateIDA 更新现有IDA的指定字段。
	// 需要持有者的权限，并在成功时发出 IDAUpdated 事件。
	// 如果IDA成功更新，返回 true 以及可能的错误代码。
	UpdateIDA(key string, field string, updates []byte, updateAt time.Time) protogo.Response

	// DeleteIDAs 根据它们的ID删除一个或多个IDA。
	// 需要持有者的权限，并在成功时发出 IDADeleted 事件。
	// 如果IDAs成功删除，返回 true 以及可能的错误代码。
	DeleteIDAs(IDs []string) protogo.Response

	// ApplyForCertification 提交数据资产的认证申请。
	// 需要认证权限，并在成功时发出 IDACertificationApplied 事件。
	// 如果认证申请成功提交，返回 true 以及任何错误。
	// `authenticatorAddress` 参数是申请认证的用户的区块链地址。
	ApplyForCertification(ID, authenticatorAddress, certificationCategory string) protogo.Response

	// CertifyIDA 根据提供的认证详情对IDA进行合规认证。
	// 需要认证权限，并在成功时发出 IDACertified 事件。
	// 如果IDA成功认证，返回 true 以及可能的错误代码。
	CertifyIDA(ID string, certification CertificationInfo) protogo.Response

	// QueryIDA 根据其ID和指定字段检索IDA的信息。
	// 如果 field 为空，则返回有关IDA的完整信息。
	// 返回请求的IDA信息和可能的错误代码。
	QueryIDA(ID string, field string) protogo.Response
}

// PlatformInfo 平台信息
type PlatformInfo struct {
	Name            string    `json:"name"`            // 平台名称
	Pubkey          string    `json:"pubkey"`          // 平台公钥，base64 编码字符串
	CreatedAt       time.Time `json:"created_at"`      // 创建时间
	Address         string    `json:"address"`         // 平台对应的链账户地址
	EnterpriseName  string    `json:"enterpriseName"`  // 企业名称
	EnterpriseTypes []int     `json:"enterpriseTypes"` // 使用平台的机构或者企业类型
	CreditCode      string    `json:"creditCode"`      // 企业信用编码
}

// UserInfo 定义了用户结构
type UserInfo struct {
	CreatedAt      *time.Time      `json:"created_at"`          // 创建时间
	Address        string          `json:"address"`             // 用户地址
	UserType       int             `json:"userType"`            // 用户类型，0: 管理员用户, 1: 普通用户
	UserPermission int             `json:"userPermission"`      // 用户权限
	IDAAmount      int             `json:"ida_amount"`          // 用户持有的IDA数量
	Approvals      map[string]bool `json:"approvals,omitempty"` // 授权方的链上地址列表
	PlatformPubkey string          `json:"platformPubkey"`      // 关联的平台公钥，base64 编码字符串
}

// IDAInfo 定义了数据资产的主要结构体
type IDAInfo struct {
	Basic          Basic               `json:"basic"`
	Ownership      Ownership           `json:"ownership"`
	Source         Source              `json:"source,omitempty"`
	Scenarios      Scenarios           `json:"scenarios,omitempty"`
	Supply         Supply              `json:"supply,omitempty"`
	Details        Details             `json:"details,omitempty"`
	Status         Status              `json:"status,omitempty"`
	Columns        []ColumnInfo        `json:"columns,omitempty"`
	APIs           []APIInfo           `json:"apis,omitempty"`
	Certifications []CertificationInfo `json:"certifications,omitempty"`
}

// Basic 定义了数据资产的基本信息结构
type Basic struct {
	ID              string       `json:"id"`                    // 资产的唯一登记编号
	Name            string       `json:"name"`                  // 资产的名称
	EnName          string       `json:"enName"`                // 资产英文名称
	Tags            []string     `json:"tags,omitempty"`        // 资产相关的关键词
	FileAttachments []Attachment `json:"attachments,omitempty"` // 资产相关的文件附件
	Category        int          `json:"category"`              // 资产的类型，1: 数据集, 2: API服务, 3: 数据报告, 4: 数据应用, 5: 计算模型
	Industry        Industry     `json:"industry"`              // 资产所属的行业代码
	Summary         string       `json:"summary,omitempty"`     // 资产的简介
	Creator         string       `json:"creator,omitempty"`     // 资产创建者的链上地址
	TxID            string       `json:"txID,omitempty"`        // 资产创建的交易ID
}

// Attachment 定义了附件资料的结构
type Attachment struct {
	Hash      string     `json:"hash,omitempty"`       // 资料哈希值
	Url       string     `json:"url,omitempty"`        // 资料的URL地址
	Type      int        `json:"type,omitempty"`       // 附件类型，1: 图片, 2: 合规证明材料, 3: 估值证明材料, 4: 其他相关附件
	Size      int        `json:"size,omitempty"`       // 资料的文件大小
	CreatedAt *time.Time `json:"created_at,omitempty"` // 附件创建时间
	UpdatedAt *time.Time `json:"updated_at,omitempty"` // 附件更新时间
	DeletedAt *time.Time `json:"deleted_at,omitempty"` // 附件删除时间
	Auditor   string     `json:"auditor,omitempty"`    // 审计者的链上地址
}

// Industry 定义了行业分类的结构
type Industry struct {
	Id    int    `json:"id"`              // id
	Code  string `json:"code,omitempty"`  // 行业编号
	Title string `json:"title,omitempty"` // 行业名称
}

// Ownership 定义了数据资产的所有权和使用情况
type Ownership struct {
	Holder    string          `json:"holder"`              // 资产持有者的链上地址
	Users     map[string]bool `json:"users,omitempty"`     // 资产加工使用者的链上地址列表
	Operators map[string]bool `json:"operators,omitempty"` // 资产经营者的链上地址列表
}

// Source 定义了数据资产来源的结构
type Source struct {
	Name             string       `json:"name,omitempty"`         // 源数据的名称
	Type             int          `json:"type,omitempty"`         // 数据的取得方式，1: 原始取得, 2: 收集取得, 3: 交易取得, 4: 其他方式
	Channel          string       `json:"channel,omitempty"`      // 数据的来源渠道
	ProofAttachments []Attachment `json:"attachments,omitempty"`  // 取得方式的证明文件
	UpdateCycle      UpdateCycle  `json:"update_cycle,omitempty"` // 数据的更新周期
}

// UpdateCycle 定义了数据资产的更新周期结构
type UpdateCycle struct {
	UpdateCycleType int    `json:"update_cycle_type"`           // 更新周期类型，1: 静态, 2: 实时, 3: 周期, 4：其他
	Cycle           int    `json:"cycle,omitempty"`             // 更新周期的具体数值
	UpdateCycleUnit int    `json:"update_cycle_unit,omitempty"` // 更新周期的单位，1: 分钟, 2: 小时, 3: 天
	Cron            string `json:"cron,omitempty"`              // 可扩展更新字段，可以使用cron表达式
}

// Scenarios 定义了数据资产适用的场景
type Scenarios struct {
	ApplicableScenarios string `json:"applicable_scenarios,omitempty"` // 适用场景描述
	ProhibitedScenarios string `json:"prohibited_scenarios,omitempty"` // 禁用场景描述
}

// Supply 定义了数据资产供应的结构
type Supply struct {
	ImmediatelySupply bool       `json:"immediately_supply,omitempty"`  // 是否即时供应
	DelayedSupplyTime *time.Time `json:"delayed_supply_time,omitempty"` // 延迟供应时间
}

// Details 定义了数据资产的具体细节
type Details struct {
	UserCategories []int      `json:"user_categories,omitempty"` // 使用对象类别，1: 政府用户, 2: 企业用户, 3: 个人用户, 4: 无限制用户
	Description    string     `json:"description,omitempty"`     // 资产的详细描述
	DataSample     string     `json:"data_sample,omitempty"`     // 数据产品的示例
	DataFormat     int        `json:"data_format,omitempty"`     // 数据的存储格式，1: 数据表格式, 2: Excel格式, 3: XML格式, 4: CSV格式, 5: JSON格式, 9: 其他格式
	TimeSpan       string     `json:"time_span,omitempty"`       // 数据的时间跨度
	CustomInfo     string     `json:"custom_info,omitempty"`     // 个性化信息汇总
	DataScale      DataScale  `json:"data_scale,omitempty"`      // 数据的规模描述
	ExpirationTime *time.Time `json:"expiration_time,omitempty"` // 资产的失效时间
}

// DataScale 定义了数据资产的规模
type DataScale struct {
	Type  int `json:"type"`  // 数据规模类型，1: 条, 2: M, 3: G
	Scale int `json:"scale"` // 数据规模数量
}

// Status 定义了数据资产的当前状态
type Status struct {
	Status              int        `json:"status"`                         // 资产的当前状态，1: 已创建, -1: 已删除
	CertificationStatus int        `json:"certification_status,omitempty"` // 认证状态，0: 待认证, 1: 认证中, 2: 认证完成
	CreatedAt           *time.Time `json:"created_at,omitempty"`           // 资产的创建时间
	UpdatedAt           *time.Time `json:"updated_at,omitempty"`           // 资产的更新时间
	DeletedAt           *time.Time `json:"deleted_at,omitempty"`           // 资产的删除时间
}

// ColumnInfo 定义了数据项的结构
type ColumnInfo struct {
	Name          string `json:"name"`                     // 数据项名称
	SecurityLevel int    `json:"security_level,omitempty"` // 分级分类
	DataType      string `json:"data_type"`                // 数据类型
	DataLength    int    `json:"data_length,omitempty"`    // 数据长度
	Description   string `json:"description,omitempty"`    // 字段描述
	DataExample   string `json:"data_example,omitempty"`   // 数据样例
	CustomColumn  string `json:"custom_column,omitempty"`  // 个性化字段
	CreatedAt     string `json:"created_at,omitempty"`     // 创建时间
	UpdatedAt     string `json:"updated_at,omitempty"`     // 更新时间
	IsPrimaryKey  int    `json:"is_primary_key,omitempty"` // 是否主键
	IsNotNull     int    `json:"is_not_null,omitempty"`    // 是否非空
	PrivacyQuery  int    `json:"privacy_query,omitempty"`  // 是否隐私计算
}

// APIInfo 定义了API接口的结构
type APIInfo struct {
	Url          string `json:"url"`                  // 接口地址
	Header       string `json:"header,omitempty"`     // 请求头
	Params       string `json:"params,omitempty"`     // 请求参数
	Response     string `json:"response,omitempty"`   // 返回参数
	ResponseType string `json:"resp_type"`            // 返回类型：JSON、XML等
	Method       string `json:"method"`               // 请求方式：GET、POST等
	CreatedAt    string `json:"created_at,omitempty"` // 创建时间
	UpdatedAt    string `json:"updated_at,omitempty"` // 更新时间
}

// CertificationInfo 定义了数据资产认证的结构
type CertificationInfo struct {
	Category       int          `json:"category"`                 // 审批类型，1：合规，2：价值评估
	Result         int          `json:"result"`                   // 认证结果，0：待认证，1：通过，2：驳回
	Explains       string       `json:"explains,omitempty"`       // 驳回原因
	Description    string       `json:"description,omitempty"`    // 认证描述
	RequestAddress string       `json:"requestAddress"`           // 申请人地址
	RequestTime    *time.Time   `json:"requestTime,omitempty"`    // 申请时间
	ExpireTime     *time.Time   `json:"expireTime,omitempty"`     // 过期时间
	HandleTime     *time.Time   `json:"handleTime,omitempty"`     // 处理时间
	CreatedAt      *time.Time   `json:"createdAt,omitempty"`      // 创建时间
	UpdatedAt      *time.Time   `json:"updatedAt,omitempty"`      // 更新时间
	DeletedAt      *time.Time   `json:"deletedAt,omitempty"`      // 删除时间
	Certifications []Attachment `json:"certifications,omitempty"` // 资产的认证文件
}
