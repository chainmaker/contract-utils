package standard

const (
	// ContractStandardNameCMBC ChainMaker - Contract Standard - Base Contract
	ContractStandardNameCMBC = "CMBC"
	// ContractStandardNameCMDFA ChainMaker - Contract Standard - Digital Fungible Assets
	ContractStandardNameCMDFA = "CMDFA"
	// ContractStandardNameCMNFA ChainMaker - Contract Standard - Digital Non-Fungible Assets
	ContractStandardNameCMNFA = "CMNFA"
	// ContractStandardNameCMID ChainMaker - Contract Standard - Identity
	ContractStandardNameCMID = "CMID"
	// ContractStandardNameCMEVI  ChainMaker - Contract Standard - Evidence
	ContractStandardNameCMEVI = "CMEVI"
	// ContractStandardNameCMDID ChainMaker - Contract Standard - Decentralized Identifier
	ContractStandardNameCMDID = "CMDID"
	// ContractStandardNameCMBCS ChainMaker - Contract Standard - Blockchain Name Service
	ContractStandardNameCMBNS = "CMBNS"
	// ContractStandardNameCMBNSResolver ChainMaker - Contract Standard - Blockchain Name Service Resolver
	ContractStandardNameCMBNSResolver = "CMBNSResolver"
	// ContractStandardNameCMIDA ChainMaker - Contract Standard - Identifiable Data Asset
	ContractStandardNameCMIDA = "CMIDA"
	// ContractOther ChainMaker - other contract
	ContractOther = "OTHER"

	TrueString  = "true"
	FalseString = "false"
)

// Common Function
const (
	// ContractBCFuncStandard ChainMaker - Contract Standard - Standards
	ContractBCFuncStandard = "Standards"
)

// CMNFA Function Name
const (
	// ContractNFAFuncTokenMetadata ChainMaker - Contract CMNFA - TokenMetadata
	ContractNFAFuncTokenMetadata = "TokenMetadata"
	// ContractNFAFuncOwnerOf ChainMaker - Contract CMNFA - OwnerOf
	ContractNFAFuncOwnerOf = "OwnerOf"
	// ContractNFAFuncTotalSupply ChainMaker - Contract CMNFA - TotalSupply
	ContractNFAFuncTotalSupply = "TotalSupply"
	// ContractNFAFuncMint ChainMaker - Contract CMNFA - Mint
	ContractNFAFuncMint = "Mint"
	// ContractNFAFuncTransferFrom ChainMaker - Contract CMNFA - TransferFrom
	ContractNFAFuncTransferFrom = "TransferFrom"
)

// CMDFA Function Name
const (
	// ContractDFAFuncSymbol ChainMaker - Contract CMDFA - Symbol
	ContractDFAFuncSymbol = "Symbol"
	// ContractDFAFuncTotalSupply ChainMaker - Contract CMDFA - TotalSupply
	ContractDFAFuncTotalSupply = "TotalSupply"
	// ContractDFAFuncMint ChainMaker - Contract CMDFA - Mint
	ContractDFAFuncMint = "Mint"
	// ContractDFAFuncBurn ChainMaker - Contract CMDFA - Burn
	ContractDFAFuncBurn = "Burn"
	// ContractDFAFuncTransfer ChainMaker - Contract CMDFA - Transfer
	ContractDFAFuncTransfer = "Transfer"
	// ContractDFAFuncTransferFrom ChainMaker - Contract CMDFA - TransferFrom
	ContractDFAFuncTransferFrom = "TransferFrom"
)

// contract param name
const (
	// ContractParamAccount ChainMaker - Contract Param - account
	ContractParamAccount = "account"
	// ContractParamAmount ChainMaker - Contract Param - amount
	ContractParamAmount = "amount"
	// ContractParamFrom ChainMaker - Contract Param - from
	ContractParamFrom = "from"
	// ContractParamTo ChainMaker - Contract Param - from
	ContractParamTo = "to"
	// ContractParamTokenId ChainMaker - Contract Param - tokenId
	ContractParamTokenId = "tokenId"
)
