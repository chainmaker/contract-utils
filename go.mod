module chainmaker.org/chainmaker/contract-utils

go 1.16

require (
	chainmaker.org/chainmaker/contract-sdk-go/v2 v2.3.8
	github.com/stretchr/testify v1.8.1
)
