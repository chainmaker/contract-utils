/*
  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

  SPDX-License-Identifier: Apache-2.0
*/

package safemath

import (
	"math/big"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMaxSafeUint256(t *testing.T) {
	max := MaxSafeUint256.ToString()
	if len(max) != 78 {
		panic("err")
	}
	_, ok := SafeAdd(MaxSafeUint256, SafeUintOne)
	if ok {
		panic("err1")
	}
}
func TestParseSafeUint256(t *testing.T) {
	type args struct {
		x string
	}
	tests := []struct {
		name  string
		args  args
		want  *SafeUint256
		want1 bool
	}{
		// TODO: Add test cases.
		{
			name: "empty string",
			args: args{
				x: "",
			},
			want:  (*SafeUint256)(big.NewInt(0)),
			want1: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := ParseSafeUint256(tt.args.x)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseSafeUint256() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("ParseSafeUint256() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
func TestNewSafeUint256Equal(t *testing.T) {
	a := NewSafeUint256(100)
	b, _ := ParseSafeUint256("100")
	assert.True(t, a.Equal(b))
}
