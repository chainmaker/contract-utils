/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package safemath

import (
	"math/big"
)

// SafeUint256 calculation uint256
type SafeUint256 big.Int

var (
	SafeUintZero = (*SafeUint256)(big.NewInt(0))
	// SafeUintOne is 1
	SafeUintOne    = (*SafeUint256)(big.NewInt(1))
	MaxSafeUint256 *SafeUint256
	minSafeUint256 *SafeUint256
)

func init() {
	x := big.NewInt(1)
	x = x.Lsh(x, 256).Sub(x, big.NewInt(1))
	MaxSafeUint256 = (*SafeUint256)(x)
	minSafeUint256 = (*SafeUint256)(big.NewInt(0))
}

// ParseSafeUint256 get uint256 obj from str
func ParseSafeUint256(x string) (*SafeUint256, bool) {
	z := big.NewInt(0)
	if x == "" {
		return (*SafeUint256)(z), true
	}
	z, ok := z.SetString(x, 10)
	if !ok || z.Cmp((*big.Int)(MaxSafeUint256)) > 0 || z.Cmp((*big.Int)(minSafeUint256)) < 0 {
		return nil, false
	}
	return (*SafeUint256)(z), true
}

// NewSafeUint256  SafeUint256 constructor
// @param i
// @return *SafeUint256
func NewSafeUint256(i uint64) *SafeUint256 {
	z := big.NewInt(0)
	z.SetUint64(i)
	return (*SafeUint256)(z)
}

// SafeAdd sets z to the sum x+y and returns z.
func SafeAdd(x, y *SafeUint256) (*SafeUint256, bool) {
	z := big.NewInt(0)
	z = z.Add((*big.Int)(x), (*big.Int)(y))
	if z.Cmp((*big.Int)(MaxSafeUint256)) > 0 {
		return nil, false
	}
	return (*SafeUint256)(z), true
}

// SafeSub sets z to the difference x-y and returns z.
func SafeSub(x, y *SafeUint256) (*SafeUint256, bool) {
	if (*big.Int)(x).Cmp((*big.Int)(y)) < 0 {
		return nil, false
	}
	return (*SafeUint256)((*big.Int)(x).Sub((*big.Int)(x), (*big.Int)(y))), true
}

// SafeMul sets z to the product x*y and returns z.
func SafeMul(x, y *SafeUint256) (*SafeUint256, bool) {
	z := (*big.Int)(x).Mul((*big.Int)(x), (*big.Int)(y))
	if z.Cmp((*big.Int)(MaxSafeUint256)) > 0 || z.Cmp((*big.Int)(minSafeUint256)) < 0 {
		return nil, false
	}
	return (*SafeUint256)(z), true
}

// SafeDiv sets z to the quotient x/y for y != 0 and returns z.
// If y == 0, a division-by-zero run-time panic occurs.
// Div implements Euclidean division (unlike Go); see DivMod for more details.
func SafeDiv(x, y *SafeUint256) *SafeUint256 {
	return (*SafeUint256)((*big.Int)(x).Div((*big.Int)(x), (*big.Int)(y)))
}

// Cmp compares x and y and returns:
//
//   -1 if x <  y
//    0 if x == y
//   +1 if x >  y
//
func (x *SafeUint256) Cmp(y *SafeUint256) int {
	return (*big.Int)(x).Cmp((*big.Int)(y))
}

// ToString get str from uint256
func (x *SafeUint256) ToString() string {
	return (*big.Int)(x).String()
}

// IsMaxSafeUint256 return is maxUint256
func (x *SafeUint256) IsMaxSafeUint256() bool {
	return x.Cmp(MaxSafeUint256) == 0
}

// GTE if x>=y return true
func (x *SafeUint256) GTE(y *SafeUint256) bool {
	return (*big.Int)(x).Cmp((*big.Int)(y)) >= 0
}

// Equal if x==y return true
// @param y
// @return bool
func (x *SafeUint256) Equal(y *SafeUint256) bool {
	return (*big.Int)(x).Cmp((*big.Int)(y)) == 0
}
